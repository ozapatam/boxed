FROM cern/cc7-base:20180516

RUN yum -y install \
        git \
        php \
        rh-php71* \
        mariadb \
        nodejs \
        npm \
        make \
        wget \
        bzip2

RUN mkdir -p /var/www/html
RUN cd /var/www/html && \
        git clone https://github.com/cernbox/core cernbox && \
        cd cernbox && \
        git checkout reva && \
        git submodule update --init

RUN source /opt/rh/rh-php71/enable && \
        npm install -g yarn && \
        cd /var/www/html/cernbox && \
        make