#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."


    ### Verify relevant environment variables are set
    if [ -z ${EOS_MGM_ALIAS} ] || [ -z ${EOS_MQ_ALIAS} ] ; then
      echo "ERROR: EOS aliases for MGM and MQ are not set."
      echo "Cannot continue."
      exit -1
    fi


    ### Restore backups when using hostPath in Kubernetes ###
    # Set permissions and ownership on eos folders. Such folders should be saved on persistent volumes!
    # Note: All directories will already exist (as mounted with persistent volumes)
    #       Check if they are empty and, if so, set attributes and copy back the file/folders wanted by eos
    LOG_PATH="/var/log/eos"
    if [ ! "$(ls -A $LOG_PATH)" ]; then
      echo "Configuring directory for logs..."
      chown -R daemon:daemon $LOG_PATH
      chmod -R 755 $LOG_PATH
      # If there is a backup (e.g., to preserve 'tx' subfolder), put it back in place
      if [ -d "/tmp/var-log-eos" ]; then
        cp -p -r /tmp/var-log-eos/* $LOG_PATH
      fi
    fi

    CONFIG_PATH="/var/eos"
    if [ ! "$(ls -A $CONFIG_PATH)" ]; then
      echo "Configuring directory for eos config and namespace..."
      chown daemon:daemon $CONFIG_PATH
      chmod 700 $CONFIG_PATH
      # If there is a backup (e.g., to preserve 'html' and 'wfe' subfolders), put it back in place
      if [ -d "/tmp/var-eos" ]; then
        cp -p -r /tmp/var-eos/* $CONFIG_PATH
      fi
    fi


    ### Check Network Connectivity ###
    # Wait for Kubernetes service to be set up properly
    # If the eos process is started right after, reverse FQDN resolution results in NXDOMAIN
    WAIT=10
    echo "Waiting ${WAIT}s before checking network configuration..."
    sleep $WAIT

    # Check the ability to resolve hostname--IP either way
    echo "Checking network configuration..."
    # Host FQDN to IP
    RESOLV_FQDN=`host $(hostname --fqdn)`
    if [ $? -ne 0 ]; then
      echo "ERROR: Unable to resolve hostname: $RESOLV_FQDN"
      echo "Cannot continue."
      exit -1
    fi
    # IP to host FQDN
    RESOLV_IP=`host $(echo $RESOLV_FQDN | rev | cut -d ' ' -f 1 | rev)`
    if [ $? -ne 0 ]; then
      echo "ERROR: Unable to resolve IP address: $RESOLV_IP"
      echo "Cannot continue."
      exit -1
    fi
    # Check bijection between Host FQDN and IP address
    if [ `hostname --fqdn` != `echo $RESOLV_IP | rev | cut -c 2- | cut -d ' ' -f 1 | rev` ]; then
      echo "ERROR: Host FQDN and IP address do not satisfy one-to-one correspondece"
      echo $RESOLV_FQDN
      echo $RESOLV_IP
      echo "Cannot continue."
      exit -1
    fi


    ### Start crond for logrotation in supervisor ###
    echo "Enabling crond for logrotation..."
    mv /etc/supervisord.d/crond.noload /etc/supervisord.d/crond.ini


    ### Configure container accoridng to the EOS role ###
    echo "Deploying eos node with role: $EOS_ROLE"
    case `echo $EOS_ROLE | tr '[:upper:]' '[:lower:]'` in
      "mgm")
        # Env vars specific to mgm
        if [ -z ${EOS_MGM_MASTER1} ] || [ -z ${EOS_MGM_MASTER2} ] ; then
          echo "ERROR: EOS MASTER1 and MASTER2 FQDNs are not set."
          echo "Cannot continue."
          exit -1
        fi

        # Enable the mgm process in supervisor
        mv /etc/supervisord.d/xrootd_mgm.noload /etc/supervisord.d/xrootd_mgm.ini

        # Configure and enable nscd and nslcd services
        sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/nslcd.conf
        mv /etc/supervisord.d/nscd.noload /etc/supervisord.d/nscd.ini
        mv /etc/supervisord.d/nslcd.noload /etc/supervisord.d/nslcd.ini

        # Check if the mq process should run in the same container
        if [ "$EOS_MGM_ALIAS" == "$EOS_MQ_ALIAS" ]; then
          echo "WARNING: MGM and MQ aliases match."
          echo "Assuming both mq and mgm processes must run in this container..."
          mv /etc/supervisord.d/xrootd_mq.noload /etc/supervisord.d/xrootd_mq.ini
        fi

        # Check if there is an EOS configuration in place, or bootstrap the instace
        if [ ! -d "/var/eos/config" ]; then
          echo "WARNING: Configuration for EOS not found."
          echo "WARNING: Scripts to bootstrap the instance will be executed."

          if [ -z ${EOS_PREFIX} ]; then
            export EOS_PREFIX="/eos/docker/user/"
            echo "WARNING: EOS_PREFIX not set. Defaulting to $EOS_PREFIX"
          fi
          if [ -z ${EOS_METADATADIR} ]; then
            export EOS_METADATADIR="/eos/docker/user/.sys.dav.hide#.user.metadata/"
            echo "WARNING: EOS_METADATADIR not set. Defaulting to $EOS_METADATADIR"
          fi
          if [ -z ${EOS_RECYCLEDIR} ]; then
            export EOS_RECYCLEDIR="/eos/docker/proc/recycle/"
            echo "WARNING: EOS_RECYCLEDIR not set. Defaulting to $EOS_RECYCLEDIR"
          fi
          if [ -z ${EOS_BASE} ]; then
            export EOS_BASE="/eos/docker/"
            echo "WARNING: EOS_BASE not set. Defaulting to $EOS_BASE"
          fi

          mv /etc/supervisord.d/bootstrap_instance_config.noload /etc/supervisord.d/bootstrap_instance_config.ini
        fi
      ;;

      "mq")
        # Enable the mq process in supervisor
        mv /etc/supervisord.d/xrootd_mq.noload /etc/supervisord.d/xrootd_mq.ini
      ;;

      "fst")
        # Env vars specific to fst
        if [ -z ${FST_MOUNTPOINT} ]; then
          echo "ERROR: FST_MOUNTPOINT not set."
          echo "ERROR: I do not know where to write data."
          echo "Cannot continue."
          exit -1
        fi

        ### TODO
	# This shold be double-checked. 
	# It would be better to enforce to have the directory instead of creating a new one.
        if  [ ! -d "$FST_MOUNTPOINT" ]; then
          echo "WARNING: The data directory ${FST_MOUNTPOINT} does not exists."
          echo "WARNING: Please check your configuration."
          echo "If you intend to write data on a persistent volume, this is a symptom of misconfiguration."
          echo "If you intend to write to a volatile scratch space, you can ignore this message." 
          mkdir -p ${FST_MOUNTPOINT}
        fi

        if [ -z ${FST_GEOTAG} ]; then
          export FST_GEOTAG="docker"
          echo "WARNING: FST_GEOTAG not set. Defaulting to $FST_GEOTAG"
        fi
        if [ -z ${FST_SCHEDULING_GROUP} ]; then
          export FST_SCHEDULING_GROUP="default"
          echo "WARNING: FST_SCHEDULING_GROUP not set. Defaulting to $FST_SCHEDULING_GROUP"
        fi
        if [ -z ${FST_FQDN} ]; then
          echo "WARNING: FST_FQDN not set."
          echo "WARNING: Defaulting to the FQDN inferred from the container and hoping for the best..."
          export FST_FQDN=`hostname --fqdn`
          echo "FST FQDN is: ${FST_FQDN}"
        fi

        # Enable the fst process in supervisor
        mv /etc/supervisord.d/xrootd_fst.noload /etc/supervisord.d/xrootd_fst.ini
        mv /etc/supervisord.d/register_fs.noload /etc/supervisord.d/register_fs.ini
      ;;

      *)
        echo "ERROR: Invalid EOS role."
        echo "Cannot continue."
        exit -1
    esac
    ;;

  ###
  "compose")
    echo "Deploying with configuration for Docker Compose..."

    # Wait for the mgm lock to be removed (controlled by ldap, addusers.sh)
    # There is no point in starting other components without the mgm up and configured
    while [[ -f ${HOST_FOLDER}/eos-mgm-lock ]]
    do
      echo 'Waiting for the lock to be removed'
      sleep 10
    done

    case `echo $EOS_ROLE | tr '[:upper:]' '[:lower:]'` in
      "mgm")
        # Enable the mgm process in supervisor
        mv /etc/supervisord.d/xrootd_mgm.noload /etc/supervisord.d/xrootd_mgm.ini

        # Configure and enable nscd and nslcd services
        sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/nslcd.conf
        sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/nslcd.conf
        mv /etc/supervisord.d/nscd.noload /etc/supervisord.d/nscd.ini
        mv /etc/supervisord.d/nslcd.noload /etc/supervisord.d/nslcd.ini

        # Check if there is an EOS configuration in place, or bootstrap the instace
        if [ ! -d "/var/eos/config" ]; then
          echo "WARNING: Configuration for EOS not found."
          echo "WARNING: Scripts to bootstrap the instance will be executed."
          mv /etc/supervisord.d/bootstrap_instance_config.noload /etc/supervisord.d/bootstrap_instance_config.ini
        else
          mv /etc/supervisord.d/remove_locks.noload /etc/supervisord.d/remove_locks.ini
        fi
      ;;

      "mq")
        # Enable the mq process in supervisor
        mv /etc/supervisord.d/xrootd_mq.noload /etc/supervisord.d/xrootd_mq.ini
      ;;

      "fst")
        # Enable the fst process in supervisor
        export FST_FQDN=`hostname --fqdn`
        mv /etc/supervisord.d/xrootd_fst.noload /etc/supervisord.d/xrootd_fst.ini
        mv /etc/supervisord.d/register_fs.noload /etc/supervisord.d/register_fs.ini
      ;;

      *)
        echo "ERROR: Invalid EOS role."
        echo "Cannot continue."
        exit -1
    esac
    ;;


  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac


# Start the processes and give control to supervisord
echo "Starting services..."
/usr/bin/supervisord -c /etc/supervisord.conf

