#! /bin/bash

echo "Configuring eos namespace..."
eos attr -r set sys.forced.blockchecksum="crc32c" /eos
eos attr -r set sys.forced.blocksize="4k" /eos
eos attr -r set sys.forced.checksum="adler" /eos
eos attr -r set sys.forced.layout="replica" /eos
eos attr -r set sys.forced.maxsize="10000000000" /eos
eos attr -r set sys.forced.nstripes="2" /eos
eos attr -r set sys.forced.space="default" /eos

# EOS_PREFIX should be '/eos/docker/user/'
eos mkdir -p $EOS_PREFIX
eos attr set sys.mask="700" $EOS_PREFIX
eos attr set sys.owner.auth="*" $EOS_PREFIX

# EOS_METADATADIR should be '/eos/docker/user/.sys.dav.hide#.user.metadata/'
eos mkdir -p $EOS_METADATADIR
eos attr set sys.mask="700" $EOS_METADATADIR
eos attr set sys.owner.auth="*" $EOS_METADATADIR
eos chmod 700 $EOS_METADATADIR

for a in {a..z} {0..9}; do
    eos mkdir -p $EOS_PREFIX$a
    eos mkdir -p $EOS_METADATADIR$a
done

echo "Done."
