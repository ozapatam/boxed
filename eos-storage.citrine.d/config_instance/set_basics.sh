#! /bin/bash

echo "Configuring eos basics..."
eos -b vid enable sss
eos -b vid enable unix
eos -b vid set membership daemon +sudo

eos -b chmod 2700 /eos
eos -b config save -f default

echo "Done."
