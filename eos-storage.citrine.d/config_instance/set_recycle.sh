#! /bin/bash

# EOS_RECYCLEDIR sould be '/eos/docker/proc/recycle/'
# EOS_PREFIX should be '/eos/docker/user/'

echo "Configuring eos recycle bin..."
eos attr -r set sys.recycle=$EOS_RECYCLEDIR /eos
eos attr -r set sys.versioning="10" /eos
eos quota set -g 99 -v 100GB -i 1M $EOS_RECYCLEDIR
eos recycle config --lifetime 2592000
eos recycle config --add-bin $EOS_PREFIX

echo "Done."
