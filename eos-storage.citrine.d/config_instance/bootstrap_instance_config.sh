#! /bin/bash

SLEEP="30s"
echo "Waiting for xrootd process to be ready..."
echo "Sleeping for $SLEEP..."
sleep $SLEEP


echo "Bootstrapping eos instance with default configuration..."
SLEEP="5s"
SCRIPT_DIR="/root/config_instance"
SCRIPTS="set_basics.sh set_namespace.sh set_recycle.sh set_space_group.sh set_gateways.sh"

for s in $SCRIPTS
do
  echo
  echo "###"
  echo "### $s"
  /bin/bash $SCRIPT_DIR/$s
  sleep $SLEEP
done

### Removing the lock for FSTs and other services
if [ "$DEPLOYMENT_TYPE" == "compose" ]; then
  bash /root/remove_locks.sh
fi

