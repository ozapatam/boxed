#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

if [ "$#" -ne 2 ]; then
        echo "ERROR: Illegal number of parameters."
        echo "Syntax: configure_gateway.sh <action> <authentication_method>"
        echo "Example: configure_gateway.sh add unix"
        exit 1
fi

GATEWAY_ACTION=`echo $1 | tr '[:upper:]' '[:lower:]'`
GATEWAY_AUTH=`echo $2 | tr '[:upper:]' '[:lower:]'`

# Set the keytab to get sudo permissions on eos
export EOS_MGM_URL=root://$EOS_MGM_ALIAS
export XrdSecPROTOCOL=sss
export XrdSecsssKT=$EOS_GATEWAY_KEYTAB_FILE
if [ -z "$EOS_GATEWAY_KEYTAB_FILE" ]; then
  export XrdSecsssKT=/etc/eos-gateway.keytab
  echo "WARNING: EOS keytab file not set."
  echo "WARNING: Defaulting to $XrdSecsssKT"
fi


# Check if required authentication method is supported
SUPPORTED_AUTH=(krb5 gsi sss unix https)
supported=false
for auth in "${SUPPORTED_AUTH[@]}";
do
  if [ "$auth" == "$GATEWAY_AUTH" ]; then
    supported=true
    break
  fi
done
if [ ! $supported ]; then
  echo "ERROR: Required authentication method is unknown."
  echo "Supported authentication methods are: ${SUPPORTED_AUTH[@]}"
  exit -1
fi


# Check if `eos whoami` return a valid IPv4 address
function validate_ip {
  IP=$1
  stat=1

  if [[ $IP =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
    OIFS=$IFS
    IFS='.'
    IP_OCTETS=($IP)
    [[ ${IP_OCTETS==[0]} -le 255 && ${IP_OCTETS==[1]} -le 255 && ${IP_OCTETS==[2]} -le 255 && ${IP_OCTETS==[3]} -le 255 ]]
      stat=$?
    IFS=$OIFS
  fi
  return $stat
}


# Helper function
function register_gateway {
  case $1 in
    "add")
      echo "Adding gateway to eos..."
      RESULT=`eos -b -r 0 0 vid add gateway $2 $3`
      ;;

    ###
    "remove")
      echo "Removing gateway..."
      RESULT=`eos -b -r 0 0 vid remove gateway $2 $3`
      ;;

    ###
    *)
      echo "ERROR: Required action is unknown."
      echo "Supported actions are: add remove"
      exit -1
  esac

  if [ $? -ne 0 ]; then
    echo "ERROR: Unable to configure gateway $2 with authentication $3"
  fi
  echo $RESULT
}


# Register current node as EOS gateway
WHOAMI=`eos -b whoami | rev | cut -d '=' -f 1 | rev`
register_gateway $GATEWAY_ACTION $WHOAMI $GATEWAY_AUTH

###
# NOTE: This is a workaround
#
# The ability to configure the same node as gateway in both IPv4 and IPv6 address
# formats can be of help in case of buggy network stacks.
# The EOS MGM process is started with the IPv4 stack only but in some cases the
# address of a client is resolve to an IPv6 adapted address, i.e., with a prepended
# "::ffff:" to the IPv4 address of the client.
# By adding both the native IPv4 the IPv6-adapted client addresses to the EOS 
# gateway list, the client is then trusted as a gateway.
if validate_ip $WHOAMI ; then
  register_gateway $GATEWAY_ACTION `echo "[::ffff:"$WHOAMI"]"` $GATEWAY_AUTH
fi

