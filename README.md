### Welcome to BOXED
*Development Repository*

-----

Self-contained, Docker-based package for next-generation cloud storage and computing services for scientific and general-purpose use:
 - EOS: https://eos.web.cern.ch
 - CERNBox: https://cernbox.web.cern.ch
 - SWAN: https://swan.web.cern.ch
 - CVMFS: https://cvmfs.web.cern.ch


Packaging by: Enrico Bocchi, Hugo Gonzalez Labrador, Jozsef Makai, Jakub T. Moscicki

-----

*Note: THIS IS A DEVELOPMENT REPOSITORY*

To deploy the services, please refer to:
  * https://github.com/cernbox/uboxed for a self-installing demo deployment
  * https://github.com/cernbox/kuboxed for a scalable and highly-available deployment with Kubernetes

For installation instruction, please refer to the GitHub repositories.

More documentation on the project is available at https://cernbox.cern.ch/cernbox/doc/boxed/

---

Copyright 2017, CERN.

AGPL License (http://www.gnu.org/licenses/agpl-3.0.html)


