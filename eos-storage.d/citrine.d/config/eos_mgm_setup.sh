#! /bin/bash

source /etc/sysconfig/eos

# not writable by daemon(???), fix needed for aquamarine
chmod 777 /var/eos/config/

# aquamarine: THE F... BLANK NEEDED HERE: -R daemon
# citrine: you may need -Rdaemon
/usr/bin/xrootd -n mgm -c /etc/xrd.cf.mgm -m -l /var/log/eos/xrdlog.mgm -b -R daemon

eos -b vid enable sss
eos -b vid enable unix
