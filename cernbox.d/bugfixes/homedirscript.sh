#!/bin/bash
# Create user's home directory when first accessing CERNBox

# Configure quota for new users
if [ -z "$USER_QUOTA_VOLUME" ]; then
  USER_QUOTA_VOLUME="20GB"
  echo "WARNING: User quota volume not specified. Defaulting to $USER_QUOTA_VOLUME physical"
fi
if [ -z "$USER_QUOTA_INODES" ]; then
  USER_QUOTA_INODES="1M"
  echo "WARNING: User quota inodes not specified. Defaulting to $USER_QUOTA_INODES"
fi


# Check input
if [ "$#" -ne 4 ]; then
	echo "illegal number of parameters"
	echo "syntax: eos-create-user-directory <eos_mgm_url> <eos_user_dir_prefix> <eos_recycle_dir_prefix> <user_id>"
	echo "example:eos-create-user-directory root://eosbackup.cern.ch /eos/scratch/user /eos/scratch/user/proc/recycle <user_id>"
	exit 1
fi

export EOS_MGM_URL=$EOS_MGM_ALIAS #Ignore the value as REVA has it hardcoded
STORAGE_PREFIX=$EOS_PREFIX  #Ignore the value as REVA has it hardcoded
RECYCLE_BIN=$EOS_PREFIX"proc/recycle"  #Ignore the value as REVA has it hardcoded
USR=$4

echo "EOS_MGM_URL=$EOS_MGM_URL, STORAGE_PREFIX=$STORAGE_PREFIX, RECYCLE_BIN=$RECYCLE_BIN, USR=$USR" >> /var/log/homescript.log

id $USR || (echo "ERROR resolving user " $USR; exit -1)
user_id=`id -u $USR`
if [ $? -ne 0 ] ; then
    echo "ERROR: Cannot retrieve user id name for user " $USR; 
    exit -1
fi
group_id=`id -g $USR`
if [ $? -ne 0 ] ; then
    echo "ERROR: Cannot retrieve group id name for user " $USR;         
    exit -1
fi

initial="$(echo $USR | head -c 1)"
homedir=$STORAGE_PREFIX/$initial/$USR
eos -b -r 0 0 mkdir -p $homedir
eos -b -r 0 0 chown -r $user_id:$group_id $homedir
eos -b -r 0 0 chmod -r 2700 $homedir
eos -b -r 0 0 attr -r set sys.acl=u:$USR:rwx\!m $homedir # not needed anymore (using sys.owner.auth) # FIXME z:!d
eos -b -r 0 0 attr -r set sys.mask="700" $homedir
eos -b -r 0 0 attr -r set sys.allow.oc.sync="1" $homedir
eos -b -r 0 0 attr -r set sys.mtime.propagation="1" $homedir 
eos -b -r 0 0 attr -r set sys.forced.atomic="1" $homedir
eos -b -r 0 0 attr -r set sys.versioning="10" $homedir
eos -b -r 0 0 quota set -u $USR -v $USER_QUOTA_VOLUME -i $USER_QUOTA_INODES -p $STORAGE_PREFIX


echo "EOS_MGM_URL=$EOS_MGM_URL, STORAGE_PREFIX=$STORAGE_PREFIX, RECYCLE_BIN=$RECYCLE_BIN, USR=$USR >> DONE" >> /var/log/homescript.log

#eos -b -r 0 0 access allow user $usr # this is temporary until we allow all users enter in
#eos -b -r 0 0 attr -r set sys.recycle="$RECYCLE_BIN" $homedir
#eos -b -r 0 0 attr -r rm sys.eval.useracl $homedir

#echo 'SUCCESS' $homedir 'created and ownership set to' $usr:$group

