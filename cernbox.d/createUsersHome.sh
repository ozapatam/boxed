#!/bin/sh

echo "Pre-creating user home folders..."

# Wait for ldap server becomes ready
until `/usr/bin/ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b $LDAP_BASE_DN &>/dev/null`
do
  date | tr '\n' ' '
  echo ": waiting for ldap..."
  sleep 2s;
done

# Create users home
for i in {0..9}; do
  echo "creating user$i"
  sh /root/createUsersHome.sh root://${EOS_MGM_ALIAS} ${EOS_PREFIX} ${EOS_RECYCLEDIR} user$i
done

echo "creating dummy_admin"
bash /root/createUsersHome.sh root://${EOS_MGM_ALIAS} ${EOS_PREFIX} ${EOS_RECYCLEDIR} dummy_admin

