#!/bin/bash
#set -o errexit # Bail out on all errors immediately

echo "---${THIS_CONTAINER}---"

case $DEPLOYMENT_TYPE in
  "kubernetes")
    # Print PodInfo
    echo ""
    echo "%%%--- PodInfo ---%%%"
    echo "Pod namespace: ${PODINFO_NAMESPACE}"
    echo "Pod name: ${PODINFO_NAME}"
    echo "Pod IP: ${PODINFO_IP}"
    echo "Node name (of the host where the pod is running): ${PODINFO_NODE_NAME}" 
    echo "Node IP (of the host where the pod is running): ${PODINFO_NODE_IP}"

    echo "Deploying with configuration for Kubernetes..."

    export MYSQL_USER=$(cat /etc/boxed/mysql_secrets/mysql_user)
    export MYSQL_PASSWORD=$(cat /etc/boxed/mysql_secrets/mysql_password)

    # Start crond for logrotation in supervisor
    echo "Enabling crond for logrotation..."
    mv /etc/supervisord.d/crond.noload /etc/supervisord.d/crond.ini

    # Self-register as gateway//quota administrator in EOS (if required)
    if [ `echo $EOS_GATEWAY_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      echo "Registering as gateway with $EOS_GATEWAY_AUTH authentication..."
      bash /root/configure_gateway.sh add $EOS_GATEWAY_AUTH
    fi
    if [ `echo $EOS_QUOTAADMIN_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      echo "Registering as quota administrator..."
      bash /root/configure_quotaadmin.sh set
    fi

    # Set permission and owner on cernbox data folder (as it should be saved on persistent volumes)
    # Note: The directory will already exist (as mounted with persistent volumes)
    #       Check if it is empty and, if so, set attributes
    DATA_PATH="/var/www/html/cernbox/data"
    if [ ! "$(ls -A $DATA_PATH)" ]; then
      echo "Configuring directory for cernbox data and shares..."
      chown -R apache:apache $DATA_PATH
      chmod -R 770 $DATA_PATH
      # If there is a backup (e.g., to preserve shares db structure), put it back in place
      if [ -d "/tmp/var-www-html-cernbox-data" ]; then
        cp -p -r /tmp/var-www-html-cernbox-data/. $DATA_PATH
      fi
    fi

    # Set database backend
    if [ -z "$DATABASE_BACKEND" ]; then
      echo "WARNING: Database backend not specified."
      echo "Defaulting to mysql as database backend..."
      export DATABASE_BACKEND="mysql"
    fi
    case $DATABASE_BACKEND in
      "mysql")
        echo "Configuring mysql (MariaDB) as database backend..."
        # Parameters to connect to MySQL should be passed as Kubernetes secrets
        sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /root/cernbox.config.mysql
        sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /root/cernbox.config.mysql
        sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /root/cernbox.config.mysql
        sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /root/cernbox.config.mysql
        sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /root/cernbox.config.mysql
        sed -i -e '/%%%DATABASE_BACKEND%%%/ {' -e 'r /root/cernbox.config.mysql' -e 'd' -e '}' /root/cernbox.config.template
        ;;
    esac
    ;;

  "compose")
    echo "Deploying with configuration for Docker Compose..."

    # Check the lock before starting the daemon
    # Note: The lock is controlled by eos-mgm, bootstrap_instance_config.sh
    while [[ -f ${HOST_FOLDER}/cernbox-lock ]]
    do
      echo "Waiting for the lock to be removed"
      sleep 10
    done

    # Set database backend
    echo "Configuring mysql as database backend..."
    sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /root/cernbox.config.mysql
    sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /root/cernbox.config.mysql
    sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /root/cernbox.config.mysql
    sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /root/cernbox.config.mysql
    sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /root/cernbox.config.mysql
    sed -i -e '/%%%DATABASE_BACKEND%%%/ {' -e 'r /root/cernbox.config.mysql' -e 'd' -e '}' /root/cernbox.config.template

    # Eventually override the certificates with the ones available in certs/boxed.{key,crt}
    if [[ -f ${HOST_FOLDER}/certs/boxed.crt && -f ${HOST_FOLDER}/certs/boxed.key ]]; then
      echo "Replacing default certificate for HTTPS..."
      /bin/cp "$HOST_FOLDER"/certs/boxed.crt /etc/boxed/certs/boxed.crt
      /bin/cp "$HOST_FOLDER"/certs/boxed.key /etc/boxed/certs/boxed.key
    fi
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac

echo "Configuring runtime parameters..."
# EOS configuration
sed -i "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /root/cernbox.config.template
sed -i "s|%%%EOS_PREFIX%%%|${EOS_PREFIX}|" /root/cernbox.config.template
sed -i "s|%%%EOS_METADATADIR%%%|${EOS_METADATADIR}|" /root/cernbox.config.template
sed -i "s|%%%EOS_RECYCLEDIR%%%|${EOS_RECYCLEDIR}|" /root/cernbox.config.template
sed -i "s|%%%EOS_PROJECTPREFIX%%%|${EOS_PROJECTPREFIX}|" /root/cernbox.config.template
#######????
sed -i "s|%%%INSTANCEID%%%|${INSTANCEID}|" /root/cernbox.config.template
sed -i "s|%%%JWTKEY%%%|${JWTKEY}|" /root/cernbox.config.template

# LDAP configuration
sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /root/cernbox.config.template
sed -i "s|%%%LDAP_PORT%%%|${LDAP_PORT}|" /root/cernbox.config.template
sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /root/cernbox.config.template
sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /root/cernbox.config.template
sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /root/cernbox.config.template

sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/nslcd.conf
sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/nslcd.conf

# Hostnames configuration
sed -i "s|%%%CERNBOXGATEWAY_HOSTNAME%%%|${CERNBOXGATEWAY_HOSTNAME}|" /root/cernbox.config.template
sed -i "s|%%%CERNBOXGATEWAY_HOSTNAME%%%|${CERNBOXGATEWAY_HOSTNAME}|" /etc/nginx/nginx.conf

# Configure services 
sed -i "s|%%%CBOXGROUPD_SECRET%%%|${CBOXGROUPD_SECRET}|" /etc/cboxgroupd/cboxgroupd.yaml
CBOXGROUPD_LDAP_URI="${LDAP_URI/ldap\:\/\//}"
sed -i "s|%%%LDAP_URI%%%|${CBOXGROUPD_LDAP_URI}|" /etc/cboxgroupd/cboxgroupd.yaml
sed -i "s|%%%LDAP_PORT%%%|${LDAP_PORT}|" /etc/cboxgroupd/cboxgroupd.yaml
sed -i "s|%%%LDAP_BASE_DN%%%|${LDAP_BASE_DN}|" /etc/cboxgroupd/cboxgroupd.yaml
sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/cboxgroupd/cboxgroupd.yaml
sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/cboxgroupd/cboxgroupd.yaml

sed -i "s|%%%CBOXSWANAPID_SECRET%%%|${CBOXSWANAPID_SECRET}|" /etc/cboxswanapid/cboxswanapid.yaml
sed -i "s|%%%CBOXSWANAPID_SIGNKEY%%%|${CBOXSWANAPID_SIGNKEY}|" /etc/cboxswanapid/cboxswanapid.yaml
sed -i "s|%%%CBOXGROUPD_SECRET%%%|${CBOXGROUPD_SECRET}|" /etc/cboxswanapid/cboxswanapid.yaml
sed -i "s|%%%SWAN_BACKEND%%%|${SWAN_BACKEND}|" /etc/cboxswanapid/cboxswanapid.yaml

sed -i "s|%%%CBOXGROUPD_SECRET%%%|${CBOXGROUPD_SECRET}|" /etc/ocproxy/ocproxy.yaml
sed -i "s|%%%JWTKEY%%%|${JWTKEY}|" /etc/ocproxy/ocproxy.yaml
sed -i "s|%%%WOPI_SECRET%%%|${WOPI_SECRET}|" /etc/ocproxy/ocproxy.yaml
sed -i "s|%%%WOPI_SERVER%%%|${WOPI_SERVER}|" /etc/ocproxy/ocproxy.yaml
sed -i "s|%%%CERNBOXGATEWAY_HOSTNAME%%%|${CERNBOXGATEWAY_HOSTNAME}|" /etc/ocproxy/ocproxy.yaml

sed -i "s|%%%JWTKEY%%%|${JWTKEY}|" /etc/revad/revad.yaml
sed -i "s|%%%LDAP_URI%%%|${LDAP_URI}|" /etc/revad/revad.yaml
sed -i "s|%%%LDAP_PORT%%%|${LDAP_PORT}|" /etc/revad/revad.yaml
sed -i "s|%%%LDAP_BIND_DN%%%|${LDAP_BIND_DN}|" /etc/revad/revad.yaml
sed -i "s|%%%LDAP_BIND_PASSWORD%%%|${LDAP_BIND_PASSWORD}|" /etc/revad/revad.yaml
sed -i "s|%%%CBOXGROUPD_SECRET%%%|${CBOXGROUPD_SECRET}|" /etc/revad/revad.yaml
sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /etc/revad/revad.yaml
sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /etc/revad/revad.yaml
sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /etc/revad/revad.yaml
sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /etc/revad/revad.yaml
sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /etc/revad/revad.yaml

sed -i "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /etc/revad/mounts.json
sed -i "s|%%%EOS_PREFIX%%%|${EOS_PREFIX}|" /etc/revad/mounts.json
sed -i "s|%%%EOS_PROJECTPREFIX%%%|${EOS_PROJECTPREFIX}|" /etc/revad/mounts.json

sed -i "s|%%%REVA_USERNAME%%%|${REVA_USERNAME}|" /etc/smashbox/smashbox.yaml
sed -i "s|%%%REVA_PASSWORD%%%|${REVA_PASSWORD}|" /etc/smashbox/smashbox.yaml

sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /etc/ocmd/ocmd.yaml
sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /etc/ocmd/ocmd.yaml
sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /etc/ocmd/ocmd.yaml
sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /etc/ocmd/ocmd.yaml
sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /etc/ocmd/ocmd.yaml
sed -i "s|%%%CERNBOXGATEWAY_HOSTNAME%%%|${CERNBOXGATEWAY_HOSTNAME}|" /etc/ocmd/ocmd.yaml

sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /etc/ocmauthd/ocmauthd.yaml
sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /etc/ocmauthd/ocmauthd.yaml
sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /etc/ocmauthd/ocmauthd.yaml
sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /etc/ocmauthd/ocmauthd.yaml
sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /etc/ocmauthd/ocmauthd.yaml

sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /etc/oauthauthd/oauthauthd.yaml
sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /etc/oauthauthd/oauthauthd.yaml
sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /etc/oauthauthd/oauthauthd.yaml
sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /etc/oauthauthd/oauthauthd.yaml
sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /etc/oauthauthd/oauthauthd.yaml

sed -i "s|%%%MYSQL_DB%%%|${MYSQL_DB}|" /etc/cboxshareadmin.ini
sed -i "s|%%%MYSQL_URI%%%|${MYSQL_URI}|" /etc/cboxshareadmin.ini
sed -i "s|%%%MYSQL_PORT%%%|${MYSQL_PORT}|" /etc/cboxshareadmin.ini
sed -i "s|%%%MYSQL_USER%%%|${MYSQL_USER}|" /etc/cboxshareadmin.ini
sed -i "s|%%%MYSQL_PASSWORD%%%|${MYSQL_PASSWORD}|" /etc/cboxshareadmin.ini
sed -i "s|%%%EOS_MGM_ALIAS%%%|${EOS_MGM_ALIAS}|" /etc/cboxshareadmin.ini
sed -i "s|%%%EOS_PREFIX%%%|${EOS_PREFIX}|" /etc/cboxshareadmin.ini
sed -i "s|%%%EOS_PROJECTPREFIX%%%|${EOS_PROJECTPREFIX}|" /etc/cboxshareadmin.ini
sed -i "s|%%%EOS_RECYCLEDIR%%%|${EOS_RECYCLEDIR}|" /etc/cboxshareadmin.ini
sed -i "s|%%%EOS_METADATADIR%%%|${EOS_METADATADIR}|" /etc/cboxshareadmin.ini

# Build administrators list and configure CERNBox accordingly
if [ -z "$ADMINS_LIST" ];
then
  echo "WARNING: Administrators list not defined."
  echo "Defaulting to dummy_admin."
  echo "        0 => 'dummy_admin'," > /root/cernbox.config.adminlist
else
  ADMINS_COUNT=0
  for i in $ADMINS_LIST;
  do
    echo "        $ADMINS_COUNT => '$i'," >> /root/cernbox.config.adminlist
    ADMINS_COUNT=$((ADMINS_COUNT+1))
  done
fi
echo "Administrators list:"
cat /root/cernbox.config.adminlist
sed -i -e '/%%%ADMIN_USERS%%%/ {' -e 'r /root/cernbox.config.adminlist' -e 'd' -e '}' /root/cernbox.config.template

# Put CERNBox config.php file in /var/www/html/config/config.php
cp /root/cernbox.config.template /var/www/html/cernbox/config/config.php
chown -R apache:apache /var/www/html/cernbox/config/


###
## Create user's home folder
#echo "Creating user's home folders..."
#bash /root/createUsersHome.sh
###


# Configuring HTTPS port for ssl traffic
if [ -z "$HTTPS_PORT" ]; then
  echo "Defaulting to port 443 for HTTPS traffic..."
  export HTTPS_PORT=443
else
  echo "Setting listening port for HTTPS traffic to $HTTPS_PORT..."
fi
echo "CONFIG: HTTP port is not used. Serving web traffic only over HTTPS"
echo "CONFIG: HTTPS port is ${HTTPS_PORT}"
sed -i "s|%%%HTTPS_PORT%%%|${HTTPS_PORT}|" /etc/nginx/nginx.conf

# Configure according to selected authentication method
if [ -z "$AUTH_TYPE" ]; then
  echo "WARNING: Authentication type not specified. Defaulting to local LDAP."
  export AUTH_TYPE="local"
fi
case $AUTH_TYPE in
  "local")
    echo "CONFIG: User authentication via LDAP"
    # TODO enable SWAN API without shib
    ;;

  "shibboleth")
    echo "CONFIG: User authentication via Shibboleth"
    echo "WARNING: Authentication via Shibboleth may required a different user backend."
    echo "WARNING: Make sure you set the right backend by replacing 'LDAPUserBackend' in '/var/www/html/cernbox/config/config.php'"
    # Enable Shibboleth rules on httpd
    mv /etc/httpd/conf.d/shib.noload /etc/httpd/conf.d/shib.conf
    # Enable shibboleth daemon
    mv /etc/supervisord.d/shibd.noload /etc/supervisord.d/shibd.ini
    # Enable SWAN API
    mv /etc/httpd/conf.d/swan.noload /etc/httpd/conf.d/swan.conf
    mv /etc/supervisord.d/cboxswanapid.noload /etc/supervisord.d/cboxswanapid.ini
    # Make sure the customization script puts in place all the remaining files
    if [ -z "$CUSTOMIZATION_REPO" ]; then
      echo "ERROR: Customization script is not set."
      echo "ERROR: The customization script should provide additional configuration files for Shibboleth authentication"
      echo "Cannot continue."
      exit 1
    echo "CONFIG: Plese make sure the customization script fetches all the required files (shibboleth2.yaml, attribute-map.xml, ...) for Shibboleth authentication."
    fi
    ;;
esac

# Apply the customization script (if required)
if [ "$CUSTOMIZATION_REPO" ]; then
  CUSTOMIZATION_PATH="/tmp/customization"
  mkdir -p $CUSTOMIZATION_PATH
  echo "Fetching customizations from $CUSTOMIZATION_REPO"
  git config --global http.sslVerify false
  git clone $CUSTOMIZATION_REPO $CUSTOMIZATION_PATH
  cd $CUSTOMIZATION_PATH
  # Checkout specific commit, if set
  if [ "$CUSTOMIZATION_COMMIT" ]; then
    echo "Checkout commit $CUSTOMIZATION_COMMIT"
    git checkout $CUSTOMIZATION_COMMIT
  fi
  # Run the customization script
  if [ -z "$CUSTOMIZATION_SCRIPT" ]; then
    export CUSTOMIZATION_SCRIPT="entrypoint.sh"
  fi
  echo "Applying customizations via $CUSTOMIZATION_SCRIPT..."
  sh $CUSTOMIZATION_SCRIPT
  if [ $? -ne 0 ]; then
    echo "WARNING: Something went wrong with the customization script."
    echo "WARNING: Configuration might be inconsistent."
  fi
  cd /
fi

# Unlocking the shell for the user in the case of docker-compose
if [ "$DEPLOYMENT_TYPE" == "compose" ]; then
  echo "Unlocking the shell at the end of the deployment."
  rm ${HOST_FOLDER}/usercontrol-lock
fi


# Start services
echo "Starting services..." 
/usr/bin/supervisord -c /etc/supervisord.conf