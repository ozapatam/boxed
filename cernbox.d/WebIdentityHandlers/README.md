## Web Identity Handlers for CERNBox
These instructions illustrate how to use custom user backends specifically developed to map Web identities to Unix accounts and to store such mapping in a dedicated LDAP server. The mapping between Web identities and Unix accounts must be considered internal to Boxed as the provider of Web identities (e.g., a Single Sign-On solution) will have neither consciousness of the mapping nor visibility on it.
User information and mapping stored in LDAP can be queried by any other service in Boxed (e.g., EOS and SWAN) so that all services have a consistent view of users accessing the system.

Then using such custom backends, CERNBox becomes the service responsible for identifying web users and for storing their Unix entry and mapping in the LDAP server. Users will have to log in to CERNBox first so that they can be registered in the system.


### Requirements
  * Shibboleth
  * LDAP server version "v0.1" at least (custom schemas required)


### UserBackendSSOtoLDAP
UserBackendSSOtoLDAP allows to create a mapping between any Web Identity with a unique UID (alphanumerical) and a dedicated Unix account. Three different elements are saved in the LDAP server:
  * The Web metadata (ssoAttributes): Web UID --required--, given name, surname, display name, email address. The four last are replaced with "--" if not provided;
  * The Unix Account information (unixAccount): uid, uidNumber, gidNumber --required--, common name, description, organization, organizational unit, homeDirectory, loginShell, gecos, userPassword;
  * The mapping information between the two (ssoUnixMatch): Web UID, uid, uidNumber, gidNumber --required--, plus the other parameters defined for Web metadata and the Unix Account if required by the application.

All the metadata related to the Web Identity are expected to be provided by Shibboleth and made available via the 'SERVER' array (e.g., `$_SERVER['uid']`)

The mapping is based on the order of arrival of users: When a user logs in to CERNBox for the first time, the three entries are created and stored in the LDAP server via the script `webuser_to_ldap.sh` and a unique uidNumber is attributed to the user. Such number is defined as the number of entries stored in the LDAP server, increased by one. The Unix Account information of a user can be customized by setting the variables 'NAME_PREFIX' and 'UIDNUMBER_START' in the `webuser_to_ldap.sh` script.
If a user is already known to the system, the Unix Account information will be retrieved from LDAP via the script `ldap_lookup.sh`.
The retrieval of additional information (e.g., Display name, email address, etc.) is deferred to dedicated functions (e.g., `get_display_name.sh`).


### UserBackendSSOtoLDAPNumericUID
UserBackendSSOtoLDAPNumericUID is a simplified version of UserBackendSSOtoLDAP where no mapping is performed.
Such User Backend is based on the assumption that the Web identity provider issues Web UIDs in a format that can be directly used as a Unix uidNumber.

In this case, the user account information is stored in a single element (ssoUnixMatch) in LDAP.
The mechanism to store new accounts and retrieve information for already-existing ones in left unchanged with respect to UserBackendSSOtoLDAP.

