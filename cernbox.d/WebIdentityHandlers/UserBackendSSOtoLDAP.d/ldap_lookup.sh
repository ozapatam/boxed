#!/bin/bash

# WARNING: THIS IS USED TO LOOKUP FOR USERS IN THE LDAP SERVER


### CLI parameters
if [ "$#" -ne 3 ]; then
  echo "Illegal number of parameters"
  echo "syntax: $0 <lookup_key> <lookup_value> <wanted_attribute>"
  echo "example: $0 ssouid abcd-ssouserid-1234 uid"
  exit -1
fi
LOOKUP_KEY=$1
LOOKUP_VALUE=$2
WANTED_ATTRIBUTE=$3
 
# This should be passed as env configuration of the pod
#APPROVED_KEY=mail # Make the check by email
#LIST_APPROVED="/etc/approved_mails" # Path to the text file with the list of approved emails

### Check if the user exists in ldap
USER=`ldapsearch -x -H $LDAP_URI -D $LDAP_BIND_DN -w $LDAP_BIND_PASSWORD -b $LOOKUP_KEY=$LOOKUP_VALUE,$LDAP_BASE_DN "(objectClass=ssoUnixMatch)" $APPROVED_KEY $WANTED_ATTRIBUTE -LLL`

if [ $? -ne 0 ]; then
  # Something went wrong with the LDAP search
  exit 1
fi

# To check if the user is aproved
if [ ! -z $LIST_APPROVED ]; then 
  APPROVED_VALUE=`echo "$USER" | grep "^$APPROVED_KEY: " | tr -d ' ' | cut -d ':' -f 2`
  approved_users=$(<$LIST_APPROVED)

  if [[ ! " ${approved_users[@]} " =~ $APPROVED_VALUE ]]; then
      # This user is not approved
      exit 3
  fi
fi

result=`echo "$USER" | grep "^$WANTED_ATTRIBUTE: "`

if [ `echo "$result" | wc -l` -ne 1 ]; then
  # There are too many results for that LOOKUP_VALUE
  exit 2
fi

### Return the proper matching entry, if any.
echo "$result" | tr -d ' ' | cut -d ':' -f 2
exit 0

