<?php
/**
 * Created by PhpStorm.
 * User: ebocchi
 * Date: 3/10/18
 * Time: 10:50 AM
 */

namespace OC\CernBox\Backends;


use OCP\Authentication\IApacheBackend;

class UserBackendSSOtoLDAP extends LDAPUserBackend implements IApacheBackend {

	private function validateSsoAttribute($attribute) {
		if (isset($attribute) && !empty($attribute) && is_string($attribute)){
			return $attribute;
		}
		return "--";
	}

	private function ldapLookup($ldapLookupValue){
		$lookupOK = false;
                $result = null;
                $errorCode = null;
                $ldapLookupScript = "/var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAP.d/ldap_lookup.sh";
                $ldapLookupKey = "ssouid";
                $ldapLookupWantedAttribute = "uid";

                $command = sprintf("/bin/bash %s %s %s %s", $ldapLookupScript, $ldapLookupKey, $ldapLookupValue, $ldapLookupWantedAttribute);
		exec($command, $result, $errorCode);
                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: ldap_lookup script called: command:%s returncode=%d result=%s", $command, $errorCode, $result), \OCP\Util::ERROR);
		if ($errorCode === 0) {
			$lookupOK = true;
	                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: ldaplookup script result: %s", $result[0]), \OCP\Util::ERROR);
			return $result[0];
		}
		return null;
	}

	public function isSessionActive() {

                if(isset($_SERVER['uid']) && is_string($_SERVER['uid'])) {
                        #var_dump($_SERVER['uid'], $_SERVER['displayname'], $_SERVER['givenName'], $_SERVER['sn'], $_SERVER['mail']);
                        $ldapOK = false;
                        $result = null;
                        $errorCode = null;
                        $ssoLdapAddScript = "/var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAP.d/webuser_to_ldap.sh";

                        $command = sprintf("/bin/bash %s --ssouid='%s' --displayname='%s' --name='%s' --surname='%s' --mail='%s'", $ssoLdapAddScript, self::validateSsoAttribute($_SERVER['uid']), self::validateSsoAttribute($_SERVER['displayName']), self::validateSsoAttribute($_SERVER['givenName']), self::validateSsoAttribute($_SERVER['sn']), self::validateSsoAttribute($_SERVER['mail']));
                        exec($command, $result, $errorCode);
                        \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: webuser_to_ldap script called: command:%s returncode=%d result=%s", $command, $errorCode, $result), \OCP\Util::ERROR);
                        if ($errorCode === 0) {
                                $ldapOK = true;
                                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: User successfully registred to LDAP. Continuing..."), \OCP\Util::ERROR);
                        }
                        elseif ($errorCode === 1) {
                                $ldapOK = true;
                                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: User already exists in LDAP. Continuing..."), \OCP\Util::ERROR);
                        }
                        else {
                                $ldapOK = false;
                                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: Something went wrong while registering the user in LDAP."), \OCP\Util::ERROR);
                                return null;
                        }
			$userUnixID = self::ldapLookup($_SERVER['uid']);
			return $userUnixID;
                }
                return null;
	}

	public function getLogoutAttribute() { 
		$env_sso_logout = getenv('SSO_LOGOUT_URL');
		if (isset($env_sso_logout) && !empty($env_sso_logout) && is_string($env_sso_logout)) {
			$sso_logout_url = $env_sso_logout;
		} else {
	                $sso_logout_url = "https://cernbox.web.cern.ch";
		}
		return "href=\"$sso_logout_url\"";
	}

        public function getDisplayName($uid) { 
		$result = null;
                $errorCode = null;
                $displayNameScript = "/var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAP.d/get_display_name.sh";

                $command = sprintf("/bin/bash %s %s %s", $displayNameScript, "uid", $uid);
                exec($command, $result, $errorCode);
                \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: get_display_name script called: command:%s returncode=%d result=%s", $command, $errorCode, $result), \OCP\Util::ERROR);
                if ($errorCode === 0) {
                        \OCP\Util::writeLog('files', sprintf("SSOtoLDAP: get_display_name script result: %s", $result[0]), \OCP\Util::ERROR);
                        $display_name = json_decode('"'.$result[0].'"');
                        return $display_name;
                }
		return $uid; 
	}

	public function getCurrentUserId() {
		if(isset($_SERVER['uid']) && is_string($_SERVER['uid'])) {
                        $userUnixID = self::ldapLookup($_SERVER['uid']);
                        return $userUnixID;
		}
		return null;
	}
}

