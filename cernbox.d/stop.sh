#!/bin/bash 
#set -o errexit	# Bail out on all errors immediately

case $DEPLOYMENT_TYPE in
  "kubernetes")
    echo "Stopping services on $PODINFO_NAME..."

    if [ `echo $EOS_GATEWAY_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      bash /root/configure_gateway.sh remove $EOS_GATEWAY_AUTH
    fi
    if [ `echo $EOS_QUOTAADMIN_SELF_REGISTRATION | tr '[:upper:]' '[:lower:]'` = "true" ]; then
      bash /root/configure_quotaadmin.sh remove
    fi
    ;;

  ###
  "compose")
    # Not really used
    ;;

  *)
    echo "ERROR: Deployment context is not defined."
    echo "Cannot continue."
    exit -1
esac

