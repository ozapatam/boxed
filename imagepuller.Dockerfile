### DOCKER FILE FOR imagepuller IMAGE ###

###
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/imagepuller -f imagepuller.Dockerfile .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/imagepuller
#
# NOTE: Requires access to the docker socket
###


FROM docker:stable

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>

# Copy a script to download the required docker images
ADD ./imagepuller.d/start.sh /root/
ADD ./imagepuller.d/pullimages.sh /root/

ENTRYPOINT []
CMD ["sh", "/root/start.sh"]

