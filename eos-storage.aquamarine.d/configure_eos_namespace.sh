#! /bin/bash

echo "Configuring eos namespace..."
eos attr -r set sys.forced.blockchecksum="crc32c" /eos
eos attr -r set sys.forced.blocksize="4k" /eos
eos attr -r set sys.forced.checksum="adler" /eos
eos attr -r set sys.forced.layout="replica" /eos
eos attr -r set sys.forced.maxsize="10000000000" /eos
eos attr -r set sys.forced.nstripes="2" /eos
eos attr -r set sys.forced.space="default" /eos
eos attr -r set sys.recycle="/eos/docker/proc/recycle/" /eos
eos attr -r set sys.versioning="10" /eos


echo "Creating /eos/docker/user folder and setting permissions..."
eos mkdir -p /eos/docker/user
eos attr set sys.mask="700" /eos/docker/user
eos attr set sys.owner.auth="*" /eos/docker/user

eos mkdir -p /eos/docker/user/.sys.dav.hide#.user.metadata
eos attr set sys.mask="700" /eos/docker/user/.sys.dav.hide#.user.metadata
eos attr set sys.owner.auth="*" /eos/docker/user/.sys.dav.hide#.user.metadata
eos chmod 700 /eos/docker/user/.sys.dav.hide#.user.metadata

for a in {a..z}; do
    eos mkdir -p /eos/docker/user/$a
    eos mkdir -p /eos/docker/user/.sys.dav.hide#.user.metadata/$a
done


echo "Configuring recycle bin quota node..."
eos recycle config --size 100GB
eos recycle config --lifetime 3600
eos recycle config --add-bin /eos/docker/user



: '''
echo "Setting up gateways and trusted hosts..."
eos vid add gateway cernbox unix
eos vid add gateway cernbox.demonet unix

eos vid add gateway eos-fuse unix
eos vid add gateway eos-fuse.demonet unix

eos vid add gateway cernboxgateway https
eos vid add gateway cernboxgateway.demonet https

admin_uid=`id -u dummy_admin`
admin_gid=`id -g dummy_admin`

eos vid set map -tident "*@cernbox" vuid:${admin_uid} vgid:${admin_gid}
eos vid set map -tident "*@cernbox.demonet" vuid:${admin_uid} vgid:${admin_gid}

#eos vid set map -tident "*@eos-fuse" vuid:${admin_uid} vgid:${admin_gid}
#eos vid set map -tident "*@eos-fuse.demonet" vuid:${admin_uid} vgid:${admin_gid}

eos vid set membership ${admin_uid} +sudo
'''

