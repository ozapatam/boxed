### DOCKER FILE FOR cernbox IMAGE ###

###
# export RELEASE_VERSION=":v0"
# docker build -t gitlab-registry.cern.ch/cernbox/boxedhub/cernbox${RELEASE_VERSION} -f cernbox.Dockerfile .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/cernbox/boxedhub/cernbox${RELEASE_VERSION}
####


FROM cern/cc7-base:20180516

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8


# ----- Remove yum autoupdate ----- #
RUN yum -y remove \
    yum-autoupdate


# ----- Install the basics ----- #
RUN yum -y install \
	wget \
	git \
	epel-release \
	bzip2 \
	sudo


# ----- Install tools for LDAP access ----- #
RUN yum -y install \
        nscd \
        nss-pam-ldapd \
        openldap-clients
ADD ./ldappam.d/*.conf /etc/
RUN chmod 600 /etc/nslcd.conf
ADD ./ldappam.d/nslcd_foreground.sh /usr/sbin/nslcd_foreground.sh
RUN chmod +x /usr/sbin/nslcd_foreground.sh


# ----- Install EOS client, copy the gateway keytab, and set the correct 
#       permissions to register the cernboxgateway container as UNIX gateway ----- #
ADD ./eos-storage.citrine.d/repos/*.repo /etc/yum.repos.d/
RUN yum -y install eos-client
ADD ./eos-storage.citrine.d/utils/configure_gateway.sh /root/configure_gateway.sh
ADD ./eos-storage.citrine.d/utils/configure_quotaadmin.sh /root/configure_quotaadmin.sh
ADD ./eos-storage.citrine.d/keytabs/eos-gateway.keytab /etc/eos-gateway.keytab
RUN chown daemon:daemon /etc/eos-gateway.keytab
RUN chmod 600 /etc/eos-gateway.keytab


# ----- Install httpd and related mods ----- #
RUN yum -y install \
	httpd \
	mod_ssl
RUN sed -i "s/Listen 80/#Listen 80/" /etc/httpd/conf/httpd.conf		# We only accept traffic HTTPS traffic on HTTPS_PORT
RUN mv /etc/httpd/conf.d/ssl.conf /etc/httpd/conf.d/ssl.defaults
ADD ./cernbox.d/httpd.d/ssl.conf /etc/httpd/conf.d/ssl.conf

# Copy SSL certificates
ADD ./secrets/boxed.crt /etc/boxed/certs/boxed.crt
ADD ./secrets/boxed.key /etc/boxed/certs/boxed.key


# ----- Install shibboleth ----- #
RUN yum -y install \
        shibboleth \
        opensaml-schemas \
        xmltooling-schemas \
		xmltooling-schemas \
		opensaml-bin \
		libxmltooling-devel \
		libsaml-devel
# RUN ln -s /usr/lib64/shibboleth/mod_shib_24.so /etc/httpd/modules/mod_shib_24.so
RUN mv /etc/httpd/conf.d/shib.conf /etc/httpd/conf.d/shib.defaults
ADD ./cernbox.d/httpd.d/shib.conf /etc/httpd/conf.d/shib.noload
RUN mv /etc/shibboleth/attribute-map.xml /etc/shibboleth/attribute-map.xml.defaults
RUN mv /etc/shibboleth/shibboleth2.xml /etc/shibboleth/shibboleth2.defaults

# Fix the library path for shibboleth (https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPLinuxRH6)
ENV LD_LIBRARY_PATH=/opt/shibboleth/lib64


# ----- Install PHP and required packages for CERNBox ----- #
RUN yum -y install \
	php \
	rh-php71* \
	nginx \
	nodejs \
	npm
RUN ln -s /opt/rh/httpd24/root/etc/httpd/modules/librh-php71-php7.so /etc/httpd/modules/libphp7.so 
ADD ./cernbox.d/php.d/10-php.conf /etc/httpd/conf.modules.d/10-php.conf


# ----- Install Redis ----- #
RUN yum -y install \
	redis \
	sclo-php71-php-pecl-igbinary \
	sclo-php71-php-pecl-redis


# ----- Install Python3.6 and Jupyter Notebook for SWAN viewer in CERNBox ----- #
RUN yum -y install \
	python36 \
	python36-devel \
	python36-pip \
	gcc \
	czmq \
	czmq-develm
RUN pip3.6 install nbconvert

# ----- Install package required by cboxshareadmin ----- #
# (and install python 2 devel+pip which are necessary to install it)
RUN yum -y install \
	python2-pip \
	python2-devel \
	mysql-devel && \
	pip install MySQL-python

# ----- Install Golang to develop/install personalized versions of dependencies ----- #
RUN mkdir /tmp/go && \
	cd /tmp/go && \
	wget https://dl.google.com/go/go1.12.linux-amd64.tar.gz && \
	tar -C /usr/local -xzf go1.12.linux-amd64.tar.gz && \
	rm -rf /tmp/go




# ----- Install CERNBox and related applications ----- #
RUN git config --global http.sslVerify false


# Install CERNBox core
RUN cd /var/www/html && \
	git clone -b reva https://github.com/cernbox/core cernbox && \
	cd cernbox && \
	git submodule update --init

# Build CERNBox
RUN source /opt/rh/rh-php71/enable && \
	npm install -g yarn && \
	yum install -y unzip && \
	cd /var/www/html/cernbox && \
	make && \
	# Force and make sure compilation went ok (by checking jquery file exists)
	cd build && /usr/bin/yarn install && \
	stat /var/www/html/cernbox/core/vendor/jquery/dist/jquery.min.js

# Install CERNBox Apps
RUN cd /var/www/html/cernbox/apps && \
	git clone -b reva https://github.com/cernbox/cernbox-theme && \
	git clone -b reva https://github.com/cernbox/files_texteditor && \
	git clone -b reva https://github.com/cernbox/files_pdfviewer && \
	git clone -b reva https://github.com/cernbox/cernboxauthtoken && \
	git clone -b reva https://github.com/cernbox/files_eostrashbin && \
	git clone -b reva https://github.com/cernbox/files_eosversions && \
	git clone -b reva https://github.com/cernbox/eosinfo && \
	git clone -b reva https://github.com/cernbox/gallery && \
	git clone -b reva https://github.com/cernbox/swanviewer && \
	git clone https://github.com/diocas/oauth2
	# git clone https://github.com/cernbox/smashbox && \
	# git clone -b reva https://github.com/cernbox/files_projectspaces && \

# Build/Install PDF viewer
RUN npm install -g bower gulp && \
	echo '{ "allow_root": true }' > /root/.bowerrc && \
	cd /var/www/html/cernbox/apps/files_pdfviewer && \
	make rebuild-pdfjs

# Configure CERNBox Apps
RUN source /opt/rh/rh-php71/enable && \
	cd /var/www/html/cernbox && \
	rm -rf config/config.php && \
	./occ maintenance:install --admin-user "_local" --admin-pass "_local"

# Disable default ones
RUN source /opt/rh/rh-php71/enable && cd /var/www/html/cernbox && \
			./occ app:disable files_trashbin  && \
			./occ app:disable files_versions  && \
			./occ app:disable comments  && \
			./occ app:disable systemtags  && \
			./occ app:disable updatenotification && \
			./occ app:disable federation && \
			./occ app:disable provisioning_api

# Enable ours
RUN source /opt/rh/rh-php71/enable && cd /var/www/html/cernbox && \
			./occ app:enable cernbox-theme && \
			./occ app:enable files_pdfviewer && \
			./occ app:enable cernboxauthtoken && \
			./occ app:enable files_eostrashbin && \
			./occ app:enable files_eosversions && \
			./occ app:enable eosinfo && \
			./occ app:enable gallery && \
			./occ app:enable swanviewer && \
			./occ app:enable oauth2
# RUN source /opt/rh/rh-php71/enable && cd /var/www/html/cernbox && ./occ app:enable smashbox


# # Remove cronjobs for sharing until pull request is accepted
# RUN sed -i 's|<job>OCA\\Files_Sharing\\DeleteOrphanedSharesJob</job>||g'  /var/www/html/cernbox/apps/files_sharing/appinfo/info.xml
# RUN sed -i 's|<job>OCA\\Files_Sharing\\ExpireSharesJob</job>||g'  /var/www/html/cernbox/apps/files_sharing/appinfo/info.xml

# # Increase maximum file upload size to 50MB
# RUN sed -i 's/upload_max_filesize = 2M/upload_max_filesize = 50M/g' /etc/opt/rh/rh-php71/php.ini
# RUN sed -i 's/post_max_size = 8M/post_max_size = 50M/g' /etc/opt/rh/rh-php71/php.ini

# Create CERNBox configuration
RUN head -n 2 /var/www/html/cernbox/config/config.php > /root/cernbox.config.template && \
	grep "passwordsalt" /var/www/html/cernbox/config/config.php >> /root/cernbox.config.template && \
	grep "secret" /var/www/html/cernbox/config/config.php >> /root/cernbox.config.template 
ADD ./cernbox.d/php.d/cernbox.config.partial /root/cernbox.config.template.partial
RUN cat /root/cernbox.config.template.partial >> /root/cernbox.config.template && \
	rm /root/cernbox.config.template.partial
ADD ./cernbox.d/php.d/cernbox.config.mysql /root/cernbox.config.mysql


# ----- Extra files required by integration with SSO solutions ----- #
ADD ./cernbox.d/WebIdentityHandlers/UserBackendSSOtoLDAP.d /var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAP.d
ADD ./cernbox.d/WebIdentityHandlers/UserBackendSSOtoLDAP.php /var/www/html/cernbox/lib/private/CernBox/Backends/UserBackendSSOtoLDAP.php
ADD ./cernbox.d/WebIdentityHandlers/UserBackendSSOtoLDAPNumericUID.d /var/www/html/cernbox/cernbox_scripts/UserBackendSSOtoLDAPNumericUID.d
ADD ./cernbox.d/WebIdentityHandlers/UserBackendSSOtoLDAPNumericUID.php /var/www/html/cernbox/lib/private/CernBox/Backends/UserBackendSSOtoLDAPNumericUID.php
ADD ./cernbox.d/WebIdentityHandlers/UserBackendSSOtoLDAPCern.php /var/www/html/cernbox/lib/private/CernBox/Backends/UserBackendSSOtoLDAPCern.php


# # ----- Workaround and temporary fixes colleection ----- #
# # TODO: Fix this!
# # Return code 22 from EOS on quota
# RUN cp /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/Commander.php /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/Commander.php.save
# ADD ./cernbox.d/bugfixes/Commander.php /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/Commander.php

# Create user home script
# RUN cp /var/www/html/cernbox/cernbox_scripts/homedirscript.sh /var/www/html/cernbox/cernbox_scripts/homedirscript.sh.save
ADD ./cernbox.d/bugfixes/homedirscript.sh /root/eosuser-homedir-creation.sh

# # Projectviewer not installed
# RUN cp /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/DBProjectMapper.php /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/DBProjectMapper.php.save
# ADD ./cernbox.d/bugfixes/DBProjectMapper.php /var/www/html/cernbox/lib/private/CernBox/Storage/Eos/DBProjectMapper.php


# Ensure tmp folders exist
RUN mkdir -p /var/tmp/eos \
			 /var/tmp/revad/tx \
			 /var/tmp/ocproxy \
			 /etc/smashbox/ \
			 /var/ocproxy-data \
			 /var/owncloud-data

RUN touch /var/owncloud-data/.ocdata && \
	mv /var/www/html/cernbox/data/owncloud.db /var/owncloud-data/

# Ensure correct permissions
RUN chown -R apache:apache /var/www/html/cernbox 
	# chown -R apache:apache /var/owncloud-data

ADD ./cernbox.d/webserverng/templates/nginx.conf.template /etc/nginx/nginx.conf
RUN chmod 0644 /etc/nginx/nginx.conf

ADD ./cernbox.d/webserverng/10-php.conf /etc/httpd/conf.modules.d/10-php.conf
RUN chmod 0644 /etc/httpd/conf.modules.d/10-php.conf

ADD ./cernbox.d/webserverng/php.ini /etc/opt/rh/rh-php71/php.ini

ADD ./cernbox.d/webserverng/php.conf /etc/httpd/conf.d/php.conf
RUN chmod 0644 /etc/httpd/conf.d/php.conf

RUN touch /var/log/owncloud.log ; \
	chown -R apache:apache /var/log/owncloud.log

ADD ./cernbox.d/webserverng/swan.conf /etc/httpd/conf.d/swan.noload

ADD ./cernbox.d/webserverng/httpd.conf /etc/httpd/conf/httpd.conf
RUN chown -R apache:apache /etc/httpd/conf/httpd.conf

# # ----- Make a copy of /var/www/html/cernbox/data (database and log) in case these will be stored on a hostPath volume ----- #
RUN mkdir -p /tmp/var-www-html-cernbox-data
RUN cp -r -p /var/www/html/cernbox/data/. /tmp/var-www-html-cernbox-data


# Create Users home script
# TODO: This should be removed as the home directory is created on-the-fly
ADD ./cernbox.d/createUsersHome.sh /root/createUsersHome.sh


# ----- Install supervisord and base configuration file ----- #
RUN yum -y install supervisor
ADD ./supervisord.d/supervisord.conf /etc/supervisord.conf

# Copy Supervisor ini files
ADD ./supervisord.d/nscd.ini /etc/supervisord.d
ADD ./supervisord.d/nslcd.ini /etc/supervisord.d
ADD ./supervisord.d/redis.ini /etc/supervisord.d
ADD ./supervisord.d/shibd.ini /etc/supervisord.d/shibd.noload
ADD ./supervisord.d/httpd.ini /etc/supervisord.d
ADD ./supervisord.d/cboxgroupd.ini /etc/supervisord.d
ADD ./supervisord.d/nginx.ini /etc/supervisord.d
ADD ./supervisord.d/revad.ini /etc/supervisord.d
ADD ./supervisord.d/ocproxy.ini /etc/supervisord.d
ADD ./supervisord.d/cboxswanapid.ini /etc/supervisord.d/cboxswanapid.noload
ADD ./supervisord.d/ocmd.ini /etc/supervisord.d
ADD ./supervisord.d/ocmauthd.ini /etc/supervisord.d
ADD ./supervisord.d/oauthauthd.ini /etc/supervisord.d
 

# ----- Run crond under supervisor and copy configuration files for log rotation ----- #
ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
ADD ./logrotate.d/logrotate /etc/cron.hourly/logrotate
RUN chmod +x /etc/cron.hourly/logrotate


# ----- Install logrotate and copy configuration files ----- #
RUN yum -y install logrotate
RUN mv /etc/logrotate.conf /etc/logrotate.defaults
ADD ./logrotate.d/logrotate.conf /etc/logrotate.conf

# Copy logrotate jobs for CERNBox
RUN rm -f /etc/logrotate.d/httpd \
    /etc/logrotate.d/httpd24-httpd
ADD ./logrotate.d/logrotate.jobs.d/httpd /etc/logrotate.d/httpd
ADD ./logrotate.d/logrotate.jobs.d/shibd /etc/logrotate.d/shibd
ADD ./logrotate.d/logrotate.jobs.d/owncloud /etc/logrotate.d/owncloud

ADD ./cernbox.d/webserverng/templates/cboxgroupd.yaml.template /etc/cboxgroupd/cboxgroupd.yaml
ADD ./cernbox.d/webserverng/templates/cboxswanapid.yaml.template /etc/cboxswanapid/cboxswanapid.yaml
ADD ./cernbox.d/webserverng/templates/ocproxy.yaml.template /etc/ocproxy/ocproxy.yaml
ADD ./cernbox.d/webserverng/templates/revad.yaml.template /etc/revad/revad.yaml
ADD ./cernbox.d/webserverng/mounts.json /etc/revad/mounts.json
ADD ./cernbox.d/webserverng/templates/ocmauthd.yaml.template /etc/ocmauthd/ocmauthd.yaml
ADD ./cernbox.d/webserverng/templates/oauthauthd.yaml.template /etc/oauthauthd/oauthauthd.yaml
ADD ./cernbox.d/webserverng/templates/ocmd.yaml.template /etc/ocmd/ocmd.yaml
# ADD ./cernbox.d/webserverng/templates/smashbox.conf.template /etc/smashbox/smashbox.conf


ENV GOPATH /root/go
ENV PATH $PATH:/usr/local/go/bin:${GOPATH}/bin

RUN mkdir -p /root/go/src/github.com/cernbox/ && \
	cd /root/go/src/github.com/cernbox/ && \
	git clone -b dev_up2u_update https://github.com/cernbox/revaold.git && \
	git clone -b dev_up2u_update https://github.com/cernbox/cboxgroupd.git && \
	git clone -b dev_up2u_update https://github.com/cernbox/ocmauthd.git && \
	git clone -b dev_up2u_update https://github.com/cernbox/ocmd.git && \
	git clone -b dev_up2u_update https://github.com/cernbox/cboxswanapid.git && \
	git clone -b dev_up2u_update https://github.com/cernbox/oauthauthd.git

RUN cd /root/go/src/github.com/cernbox/revaold/ocproxy && \
	go get && \
	go build && \
	mv ocproxy /usr/local/bin && \
	chmod +x /usr/local/bin/ocproxy && \
	mkdir /var/log/ocproxy/
RUN cd /root/go/src/github.com/cernbox/revaold/revad && \
	go get && \
	go build && \
	mv revad /usr/local/bin && \
	chmod +x /usr/local/bin/revad && \
	mkdir /var/log/revad/
RUN cd /root/go/src/github.com/cernbox/cboxgroupd && \
	go get && \
	go build && \
	mv cboxgroupd /usr/local/bin && \
	chmod +x /usr/local/bin/cboxgroupd && \
	mkdir /var/log/cboxgroupd/
RUN cd /root/go/src/github.com/cernbox/ocmauthd && \
	go get && \
	go build && \
	mv ocmauthd /usr/local/bin && \
	chmod +x /usr/local/bin/ocmauthd && \
	mkdir /var/log/ocmauthd/
RUN cd /root/go/src/github.com/cernbox/oauthauthd && \
	go get && \
	go build && \
	mv oauthauthd /usr/local/bin && \
	chmod +x /usr/local/bin/oauthauthd && \
	mkdir /var/log/oauthauthd/
RUN cd /root/go/src/github.com/cernbox/ocmd && \
	go get && \
	go build && \
	mv ocmd /usr/local/bin && \
	chmod +x /usr/local/bin/ocmd && \
	mkdir /var/log/ocmd/
RUN cd /root/go/src/github.com/cernbox/cboxswanapid && \
	go get && \
	go build && \
	mv cboxswanapid /usr/local/bin && \
	chmod +x /usr/local/bin/cboxswanapid && \
	mkdir /var/log/cboxswanapid/

# Install Python scripts
RUN cd /root && \
	git clone https://github.com/cernbox/cboxshareadmin.git
ADD ./cernbox.d/webserverng/templates/cboxshareadmin.ini.template /etc/cboxshareadmin.ini

# Clean temporary files (only doing it in the end because this image should be squashed)
RUN yum clean all && \
    rm -rf /var/cache/yum && \
	bower cache clean && \
	npm cache clean

# ----- Start the services ----- #
ADD ./cernbox.d/start.sh /root/start.sh
ADD ./cernbox.d/stop.sh /root/stop.sh
CMD ["/bin/bash","/root/start.sh"]